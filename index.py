import config
from classes.img_recognizer import ImgRecognizer
from classes.links_retriever import LinksRetriever
from classes.base64_converter import Base64Converter
import classes.exceptions


base64_converter = Base64Converter()

links_retriever = LinksRetriever('сталинка')
links = links_retriever.get_links()

img_recognizer = ImgRecognizer(
    config.driver_path,
    config.delay_sec,
    {
        'hide_phone_selector': config.hide_phone_selector,
        'show_phone_selector': config.show_phone_selector
    },
    classes.exceptions, 
    base64_converter
)

try:
    nums = img_recognizer.get_phones(links)
    print('nums', nums)
    del img_recognizer
    nums = None
except Exception as e:
    print('GET NUMS IS FAILED :: ', e)