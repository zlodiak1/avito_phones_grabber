class ErrorGetPhone(Exception):
    def __init__(self, msg):
        self.msg = msg
        

class ErrorShowPhone(Exception):
    def __init__(self, msg):
        self.msg = msg


class ErrorRecognizeImg(Exception):
    def __init__(self, msg):
        self.msg = msg


class ErrorFindSelector(Exception):
    def __init__(self, msg):
        self.msg = msg


class ErrorDriverGet(Exception):
    def __init__(self, msg):
        self.msg = msg


class ErrorDriverSuchElem(Exception):
    def __init__(self, msg):
        self.msg = msg


class ErrorDriverClickElem(Exception):
    def __init__(self, msg):
        self.msg = msg
