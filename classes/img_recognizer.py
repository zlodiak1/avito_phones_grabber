import requests

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, RemoteDriverServerException, ElementClickInterceptedException

class ImgRecognizer:
    def __init__(self, driver_path, delay_sec, selectors, exceptions, base64_converter):
        self.delay_sec = delay_sec
        self.hide_phone_selector = selectors['hide_phone_selector']
        self.show_phone_selector = selectors['show_phone_selector']
        self.driver = webdriver.Chrome(driver_path)
        self.exceptions = exceptions
        self.base64_converter = base64_converter

    def show_phone(self, link):
        try:
            self.driver.get(link);
            hide_phone_el = self.driver.find_element_by_css_selector(self.hide_phone_selector)
            hide_phone_el.click()
        except RemoteDriverServerException as e:
            raise self.exceptions.ErrorGetPhone('Error driver get() for link: ' + link + ' :: ' + e)
        except NoSuchElementException as e:
            raise self.exceptions.ErrorGetPhone('Error driver find element for link: ' + link + ' :: ' + e)
        except ElementClickInterceptedException as e:
            raise self.exceptions.ErrorGetPhone('Error driver click for link: ' + link + ' :: ' + e)
        except Exception as e:
            raise self.exceptions.ErrorGetPhone('Error show phone for link: ' + link + ' :: ' + e)

    def recognize_img(self):
        try:
            show_phone_el = WebDriverWait(self.driver, self.delay_sec).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, self.show_phone_selector))
            )
            num = self.base64_converter.get_text(show_phone_el)
            return num            
        except NoSuchElementException as e:
            raise self.exceptions.ErrorFindSelector('Error find css selector after click: ' + self.show_phone_selector + ' :: ' + e)
        except Exception as e:
            raise self.exceptions.ErrorRecognizeImg('Error recognize img after click: ' + self.show_phone_selector + ' :: ' + e)

    def get_phones(self, links):
        nums = []
        for link in links:
            num = self.get_phone(link)
            if num: nums.append(num)
        return nums

    def get_phone(self, link):
        try:
            self.show_phone(link)
            num = self.recognize_img()
            return num
        except Exception as e:
            raise self.exceptions.ErrorGetPhone('Error get phone for link: ' + link + ' :: ' + e)

    def __del__(self):
        self.driver.close()
